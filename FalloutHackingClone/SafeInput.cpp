#include "SafeInput.h"

int GetIntSafely(std::string message, int highestInputNumber) {
	int number;
	while (true) {
		std::cout << message; //Prints the message everytime it asks for input
		std::cin >> number;

		if (std::cin.fail() || number > highestInputNumber || number < 1) { //If user inputs something that isn't a number
			std::cout << "Error: Invalid input! Please select one of options 1-10" << std::endl;
			std::cin.clear(); //Clear the input stream
			std::cin.ignore(1000, '\n'); //Clear the input stream so that the user can enter a number next time the loop goes around
			continue; //Loop again for another try (actually, loops until the user enters an integer)
		}
		return number; //Return the integer which ends the loop and method, and the program can continue
	}
}
