#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <ctime>
#include <fstream>
#include <stdlib.h>

#include "SafeInput.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;


class HackGame
{
public:
	HackGame();
	~HackGame();
private:
	static const int WORD_COMPLETE_MATCH = 500;
	std::vector<string> words4, words5, words6;
	string keyword;
	int difficulty, triesLeft;
	std::vector<string> wordsInCurrentGame;
	bool first, gameRunning;

	int CompareToKeyWord(string input); //Compares user input with the keyword, and returns an int which tells the user how many letters are correct and in the correct place.
	void AddWordsToGameRound(); //Adds 10 random words to the games current round
	void AddWord(); //This function is only used by AddWordsToGameRound() to make sure it doesn't add the same word twice
	bool AlreadyAdded(string word);
	void PrintWords();
	void LoadWordsToVectors(); //Read the textfiles containing the words for the game
	void DoGameRound();
public:
	void StartGame();
};

