#include "HackGame.h"



HackGame::HackGame()
{
	LoadWordsToVectors();
}


HackGame::~HackGame()
{
}

//============================================
//				PRIVATE FUNCTIONS
//============================================

int HackGame::CompareToKeyWord(string input) {
	int matches = 0;
	if (input == keyword)
		return WORD_COMPLETE_MATCH;
	else {
		for (int i = 0; i < input.length(); i++) {
			if (input[i] == keyword[i])
				matches++;
		}
	}
	return matches;
}

void HackGame::StartGame() {
	difficulty = GetIntSafely("Please select difficulty, 1-3: ", 3);
	wordsInCurrentGame.clear();
	AddWordsToGameRound();
	triesLeft = 4;
	system("CLS"); //Clear the console before a new game

	srand(time(0));
	int random = rand() % 10;
	keyword = wordsInCurrentGame.at(random); //Set the keyword

	first = true;
	gameRunning = true;

	while (triesLeft > 0 && gameRunning) {
		DoGameRound();
	}

	bool error = false;
	while (true) {
		if (!error) {//Display this first. If user enters invalid input, show an error message instead
			if (triesLeft == 0)
				cout << endl << "Out of tries! Game Over!" << endl;
			else
				cout << endl << "You win!" << endl;
			cout << endl << "Play again? y/n: ";
		}
		else
			cout << "Error: Invalid input. Please input y or n: ";
		char input;
		cin >> input;
		if (tolower(input) == 'y')
			StartGame();
		else if (tolower(input) == 'n')
			break;
	}

}

void HackGame::DoGameRound() {
	cout << "Tries left: " << triesLeft << endl << endl;
	if (first) {
		PrintWords();
		first = false;
	}
	int input = GetIntSafely("Please select word, 1-10: ", 10);
	int matches = CompareToKeyWord(wordsInCurrentGame.at(input - 1));
	if (matches == WORD_COMPLETE_MATCH) {
		cout << "*** Input: " << wordsInCurrentGame.at(input - 1) << " | Complete match! ***" << endl;
		gameRunning = false;
	}
	else {
		triesLeft--;
		cout << endl << "*** Input: " << wordsInCurrentGame.at(input - 1) << " | Likeness: " << matches << " ***" << endl;
	}
}

void HackGame::AddWordsToGameRound() {
	srand(time(0));
	for (int i = 0; i < 10; i++)
	{
		AddWord();
	}
}
void HackGame::AddWord() {
	if (difficulty == 1) {
		int random = rand() % words4.size();
		if (!AlreadyAdded(words4.at(random)))
			wordsInCurrentGame.push_back(words4.at(random));
		else
			AddWord();
	}
	else if (difficulty == 2) {
		int random = rand() % words5.size();
		if (!AlreadyAdded(words5.at(random))) //Kolla h�r om n�got �r fel.... det �r med denna funktion iaf
			wordsInCurrentGame.push_back(words5.at(random));
		else
			AddWord();
	}
	else if (difficulty == 3) {
		int random = rand() % words6.size();
		if (!AlreadyAdded(words6.at(random)))
			wordsInCurrentGame.push_back(words6.at(random));
		else
			AddWord();
	}
}

bool HackGame::AlreadyAdded(string word) {
	for (int i = 0; i < wordsInCurrentGame.size(); i++)
	{
		if (word == wordsInCurrentGame.at(i))
			return true;
	}
	return false;
}

void HackGame::PrintWords() {
	for (int i = 0; i < wordsInCurrentGame.size(); i++) {
		cout << (i + 1) << ": " << wordsInCurrentGame.at(i) << endl;
	}
}

void HackGame::LoadWordsToVectors() {
	string line;
	std::ifstream w4("words4.txt");
	std::ifstream w5("words5.txt");
	std::ifstream w6("words6.txt");

	while (std::getline(w4, line))
		words4.push_back(line);
	while (std::getline(w5, line))
		words5.push_back(line);
	while (std::getline(w6, line))
		words6.push_back(line);
}